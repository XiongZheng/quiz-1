package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_when_guess_number() throws Exception {
        mockMvc.perform(patch("/api/games/2").content("8610"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void should_return_false_when_guess_is_incorrect() throws Exception {
        mockMvc.perform(patch("/api/games/2").content("3479"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{correct: false, hint: '0A0B'}", true));
    }

    @Test
    void should_return_true_when_guess_is_correct() throws Exception {
        mockMvc.perform(patch("/api/games/2").content("8479"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{correct: false, hint: '1A0B'}", true));
    }

    @Test
    void should_return_guess_result_when_guess_pattern_AA() throws Exception{
        mockMvc.perform(patch("/api/games/2").content("8679"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{correct: false, hint: '2A0B'}",true));
    }

    @Test
    void should_return_guess_result_when_guess_pattern_BX() throws Exception {
        mockMvc.perform(patch("/api/games/2").content("6479"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{correct: false, hint: '0A1B'}", true));
    }

    @Test
    void should_return_guess_result_when_guess_pattern_XB() throws Exception {
        mockMvc.perform(patch("/api/games/2").content("4879"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{correct: false, hint: '0A1B'}", true));
    }

    @Test
    void should_return_guess_result_when_guess_pattern_BB() throws Exception {
        mockMvc.perform(patch("/api/games/2").content("6879"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{correct: false, hint: '0A2B'}", true));
    }

    @Test
    void should_return_guess_result() {

        Map<String, String> inputs = new HashMap<>();
        // 8610
        inputs.put("8610", "4A0B");
        inputs.put("8601", "2A2B");
        inputs.put("7614", "2A0B");
        inputs.put("8790", "2A0B");

        inputs.forEach((guess, hint) -> {
            try {
                mockMvc.perform(patch("/api/games/2").content(guess))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                        .andExpect(content().json("{correct: " + hint.equals("4A0B") + ", hint: '" + hint + "'}", true));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @TestConfiguration
    static class Configuration{

        @Bean
        @Primary
        public AnswerGenerator mockAnswerGenerator(){
            return new MockAnswerGenerator();
        }
    }
    static class MockAnswerGenerator extends AnswerGenerator{

        @Override
        public String generateRandomAnswer(){
            return "8610";
        }
    }

    //test1-1
    @Test
    void should_return_status_201() throws Exception {
        mockMvc.perform(post("/api/games"))
                .andExpect(status().is(201));
    }

    //test1-2
    @Test
    void should_return_location_header() throws Exception {
        mockMvc.perform(post("/api/games"))
                .andExpect(header().string("Location", "/api/games/1"));
    }

    private AnnotationConfigApplicationContext context;

    //test2-1
    @Test
    void should_return_answer() throws Exception {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Game game = context.getBean(Game.class);
        mockMvc.perform(get("/api/games/1"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("{ \"id\":" + game.getGameId() + ", \"answer\": \"" + game.getAnswer() + "+\"}"));
    }

    //test2-2
    @Test
    void should_return_Status_404() throws Exception {
        mockMvc.perform(get("/api/games/1234"))
                .andExpect(status().is(404));
    }

    ObjectMapper objectMapper = new ObjectMapper();

    //test3.1
    @Test
    void should_return_result_when_guess_answer() throws Exception {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Game game = context.getBean(Game.class);
        Answer answer = new Answer("1234");
        mockMvc.perform(patch("/api/games/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answer)))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("{\"hint\":\"" + game.getHint(answer.getAnswer()) + "\",\"correct\":" + game.isCorrect(answer.getAnswer()) + "}"));
    }

    @Test
    void should_return_correct_result_when_guess_answer_success() throws Exception {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Game game = context.getBean(Game.class);
        Answer answer = new Answer(game.getAnswer());
        mockMvc.perform(patch("/api/games/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answer)))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("{\"hint\":\"" + game.getHint(answer.getAnswer()) + "\",\"correct\":" + game.isCorrect(answer.getAnswer()) + "}"));
    }

    @Test
    void should_return_404_when_game_id_is_not_exist() throws Exception {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Game game = context.getBean(Game.class);
        Answer answer = new Answer(game.getAnswer());
        mockMvc.perform(patch("/api/games/2333")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answer)))
                .andExpect(status().is(404));
    }

    @Test
    void should_return_400_when_game_id_is_not_number() throws Exception {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Game game = context.getBean(Game.class);
        Answer answer = new Answer(game.getAnswer());
        mockMvc.perform(patch("/api/games/game")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answer)))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_400_when_answer_is_not_4_digits() throws Exception {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Game game = context.getBean(Game.class);
        Answer answer = new Answer("12580");
        mockMvc.perform(patch("/api/games/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answer)))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_400_when_answer_contain_other_symbol_except_digit() throws Exception {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Game game = context.getBean(Game.class);
        Answer answer = new Answer("s");
        mockMvc.perform(patch("/api/games/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answer)))
                .andExpect(status().is(400));
    }
}

