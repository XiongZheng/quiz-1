package com.twuc.webApp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;

import static org.assertj.core.api.Assertions.assertThat;

public class AnswerGeneratorTest {
    private AnswerGenerator answerGenerator;

    @BeforeEach
    void setUp() {
        answerGenerator=new AnswerGenerator();
    }

    @Test
    void should_generate_answer_with_correct_length() {
        String answer = answerGenerator.generateRandomAnswer();
        assertThat(answer.length()).isEqualTo(1);
    }

    @Test
    void should_generate_answer_randomly() {
        ArrayList<String> answers = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            answers.add(answerGenerator.generateRandomAnswer());
        }

        HashSet<String> uniqueAnswers = new HashSet<>(answers);

        int countOfDuplicatedAnswer=answers.size()-uniqueAnswers.size();

        assertThat(countOfDuplicatedAnswer).isLessThan(6);
    }
}
