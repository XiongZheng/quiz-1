package com.twuc.webApp;

public class GameDouble {
    private final String answer;

    public GameDouble(String answer) {
        this.answer = answer;
    }

    public GuessResult guess(String guess) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < answer.length(); i++) {
            if (answer.charAt(i) == guess.charAt(i)) {
                stringBuilder.append("A");
                continue;
            }
            if (answer.contains(String.valueOf(guess.charAt(i)))) {
                stringBuilder.append("B");
            }
        }
        return new GuessResult(stringBuilder.toString());
    }
}