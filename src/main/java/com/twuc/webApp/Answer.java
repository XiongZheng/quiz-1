package com.twuc.webApp;

import java.util.HashSet;

public class Answer {
    private String answer;

    public String getAnswer() {
        return answer;
    }

    public Answer(String answer) {
        this.answer = answer;
    }

    public Answer() {

    }

    public boolean AnswerFormatJudge() {
        HashSet<Object> hashSet = new HashSet<>();
        for (int i = 0; i < answer.length(); i++)
            hashSet.add(answer.charAt(i));
        return answer.length() == 4 && hashSet.size() == 4;

    }
}
