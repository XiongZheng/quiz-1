package com.twuc.webApp;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class AnswerGenerator {

    private final Random random=new Random();

    public String generateRandomAnswer() {
        return String.valueOf(random.nextInt(10));
    }
}
