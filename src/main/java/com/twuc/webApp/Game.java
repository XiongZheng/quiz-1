package com.twuc.webApp;

import java.util.ArrayList;
import java.util.HashMap;

public class Game {
    private int gameId;
    private String answer;

    public int getGameId() {
        return gameId;
    }

    public String getAnswer() {
        return answer;
    }

    public Game(int gameId, String answer) {
        this.gameId = gameId;
        this.answer = answer;
    }

    public String getHint(String guessAnswer){
        int countA=0;
        int countB=0;
        HashMap<Character, Boolean> answerBitMap = new HashMap<>();
        ArrayList<Object> numberNotMatch = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            if (answer.charAt(i)==guessAnswer.charAt(i)){
                countA++;
            }else{
                answerBitMap.put(answer.charAt(i),true);
                numberNotMatch.add(guessAnswer.charAt(i));
            }
        }

        for (int i = 0; i < numberNotMatch.size(); i++) {
            if (answerBitMap.containsKey(numberNotMatch.get(i))){
                countB++;
            }
        }

        return String.format("%dA%dB",countA,countB);
    }

    public boolean isCorrect(String guessAnswer){
        return answer.equals(guessAnswer);
    }
}
