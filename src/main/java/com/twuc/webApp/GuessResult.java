package com.twuc.webApp;

import org.springframework.util.StringUtils;

public class GuessResult {
    private final boolean isCorrect;
    private final String hint;

    public GuessResult(String result) {
        this.isCorrect = result.equals("AAAA");
        int countOfA = StringUtils.countOccurrencesOf(result, "A");
        int countOfB = StringUtils.countOccurrencesOf(result, "B");
        this.hint = "" + countOfA + "A" + countOfB + "B";
    }

    @SuppressWarnings("unused")
    public boolean isCorrect() {
        return isCorrect;
    }

    @SuppressWarnings("unused")
    public String getHint() {
        return hint;
    }
}
