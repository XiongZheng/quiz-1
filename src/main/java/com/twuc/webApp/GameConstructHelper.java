package com.twuc.webApp;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Configuration
public class GameConstructHelper {
    private static Map<Integer, Game> gameMap = new HashMap<Integer, Game>();
    private int gameId;

    @Bean
    public Game createGame() {
        Random random = new Random();
        StringBuilder s = new StringBuilder();
        while (s.length() < 4) {
            int digit = random.nextInt(10);
            if (s.indexOf(String.valueOf(digit)) < 0)
                s.append(digit);
        }
        Game game = new Game(++gameId, s.toString());
        gameMap.put(gameId, game);
        return game;
    }

    public static Map<Integer, Game> getGameMap() {
        return gameMap;
    }
}
